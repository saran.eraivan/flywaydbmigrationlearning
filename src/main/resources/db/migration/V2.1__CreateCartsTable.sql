CREATE TABLE carts (
    ID int NOT NULL,
    User_ID int,
    PRIMARY KEY(ID),
    CONSTRAINT fk_User
        FOREIGN KEY (User_ID) REFERENCES users(ID)
);