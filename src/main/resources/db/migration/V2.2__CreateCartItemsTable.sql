CREATE TABLE cart_items (
    ID int NOT NULL PRIMARY KEY,
    cart_ID int references carts(ID),
    product_id int references products(ID),
    quantity int
);